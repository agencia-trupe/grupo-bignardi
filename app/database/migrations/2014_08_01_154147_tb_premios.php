<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbPremios extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('premios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('titulo');
			$table->string('subtitulo');
			$table->date('data');
			$table->string('slug');
			$table->string('olho');
			$table->text('texto');
			$table->string('imagem');
			$table->integer('ordem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('premios');
	}

}
