<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TbHome extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('home', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('frasehome');
			$table->string('subfrasehome');
			$table->string('textochamadabignardi');
			$table->string('textochamadajandaia');
			$table->string('textochamadaatacadao');
			$table->string('tituloreferencia');
			$table->text('textoreferencia');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('home');
	}

}
