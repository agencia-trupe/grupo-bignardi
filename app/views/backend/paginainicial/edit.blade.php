@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Textos da Página Inicial
        </h2>  

		{{ Form::open( array('route' => array('painel.paginainicial.update', $registro->id), 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="fraseHome">Frase</label>
					<input type="text" class="form-control" id="fraseHome" name="frasehome" value="{{$registro->frasehome}}">
				</div>
				
				<div class="form-group">
					<label for="subFrase">SubFrase</label>
					<input type="text" class="form-control" id="subFrase" name="subfrasehome" value="{{$registro->subfrasehome}}">
				</div>
				
				<div class="form-group">
					<label for="bignardiPapeis">Chamada Bignardi papéis</label>
					<input type="text" class="form-control" id="bignardiPapeis" name="textochamadabignardi" value="{{$registro->textochamadabignardi}}">
				</div>

				<div class="form-group">
					<label for="jandaia">Chamada Jandaia</label>
					<input type="text" class="form-control" id="jandaia" name="textochamadajandaia" value="{{$registro->textochamadajandaia}}">
				</div>

				<div class="form-group">
					<label for="jandaiaAtacado">Chamada Jandaia atacado de papelaria</label>
					<input type="text" class="form-control" id="jandaiaAtacado" name="textochamadaatacadao" value="{{$registro->textochamadaatacadao}}">
				</div>

				<div class="form-group">
					<label for="tituloReferencia">Título Referência</label>
					<input type="text" class="form-control" id="tituloReferencia" name="tituloreferencia" value="{{$registro->tituloreferencia}}">
				</div>

				<div class="form-group">
					<label for="textoReferencia">Título Referência</label>
					<textarea class="form-control" id="textoReferencia" name="textoreferencia" required >{{$registro->textoreferencia}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.paginainicial.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			</div>
		</form>
    </div>
    
@stop