@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.paginainicial.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="fraseHome">Frase</label>
					<input type="text" class="form-control" id="fraseHome" name="frasehome" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['frasehome'] ?>" @endif required>
				</div>
				
				<div class="form-group">
					<label for="subFrase">SubFrase</label>
					<input type="text" class="form-control" id="subFrase" name="subfrasehome" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['subfrasehome'] ?>" @endif required>
				</div>
				
				<div class="form-group">
					<label for="bignardiPapeis">Chamada Bignardi papéis</label>
					<input type="text" class="form-control" id="bignardiPapeis" name="textochamadabignardi" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['textochamadabignardi'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="jandaia">Chamada Jandaia</label>
					<input type="text" class="form-control" id="jandaia" name="textochamadajandaia" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['textochamadajandaia'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="jandaiaAtacado">Chamada Jandaia atacado de papelaria</label>
					<input type="text" class="form-control" id="jandaiaAtacado" name="textochamadaatacadao" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['textochamadaatacadao'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="tituloReferencia">Título Referência</label>
					<input type="text" class="form-control" id="tituloReferencia" name="tituloreferencia" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['tituloreferencia'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="textoReferencia">Texto Referência</label>
					<textarea class="form-control" id="textoReferencia" name="textoreferencia" required >@if(Session::has('formulario')) <?php $s=Session::get('formulario'); echo $s['textochamadaatacadao'] ?> @endif</textarea>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>
				<a href="" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop