@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Textos da Página Inicial 
        @if(!count($registros))
            <a href='{{ URL::route('painel.paginainicial.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto</a>
        @endif﻿
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='clientes'>

        <thead>
            <tr>
				<th>Frase da Home</th>
                <th>Subfrase da Home</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row">
				<td>{{ $registro->frasehome}}</td>
				<td>{{ $registro->subfrasehome}}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.paginainicial.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.paginainicial.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop