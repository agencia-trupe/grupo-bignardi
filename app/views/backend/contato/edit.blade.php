@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Textos da Página Inicial
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="endereco">Endereço</label>
					<input type="text" class="form-control" id="endereco" autofocus name="endereco" value="{{$registro->endereco}}">
				</div>

				<div class="form-group">
					<label for="enderecoRodape">Endereço do Rodape</label>
					<input type="text" class="form-control" id="enderecoRodape" name="enderecorodape" value="{{$registro->enderecorodape}}">
				</div>

				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" class="form-control" id="enderecoRodape" name="email" value="{{$registro->email}}">
				</div>

				<div class="form-group">
					<label for="telefone">Telefone</label>
					<input type="text" class="form-control" id="telefone" name="telefone" value="{{$registro->telefone}}">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			</div>
		</form>
    </div>
    
@stop