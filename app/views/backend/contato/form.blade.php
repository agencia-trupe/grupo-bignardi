@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.contato.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="endereco">Endereço</label>
					<input type="text" class="form-control" id="endereco" autofocus name="endereco" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['endereco'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="enderecoRodape">Endereço do Rodapé</label>
					<input type="text" class="form-control" id="enderecoRodape" name="enderecorodape" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['enderecorodape'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="telefone">Telefone</label>
					<input type="text" class="form-control" id="telefone" name="telefone" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['telefone'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="email">Email</label>
					<input type="text" class="form-control" id="email" name="email" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['email'] ?>" @endif required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>
				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop