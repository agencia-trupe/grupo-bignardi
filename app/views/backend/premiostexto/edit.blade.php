@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Textos da Página Inicial
        </h2>  

		{{ Form::open( array('route' => array('painel.premiostexto.update', $registro->id), 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="texto">Texto</label>
					<textarea type="text" class="form-control" id="texto" autofocus name="texto">
						{{$registro->texto}}</textarea>
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.premiostexto.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			</div>
		</form>
    </div>
    
@stop