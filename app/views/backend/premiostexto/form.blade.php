@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.premiostexto.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="texto">Texto</label>
					<textarea class="form-control" id="texto" autofocus name="texto" required>@if(Session::has('formulario')) <?php $s=Session::get('formulario'); echo $s['texto'] ?>@endif</textarea>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>
				<a href="{{URL::route('painel.premiostexto.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop