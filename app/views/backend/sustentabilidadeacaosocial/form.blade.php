@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.sustentabilidadeacaosocial.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

		    	<div class="form-group">
					<label for="titulo">Título</label>
					<input type="text" class="form-control" id="titulo" autofocus name="titulo" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['titulo'] ?>" @endif required>
				</div>

				<div class="form-group">
					<label for="texto">Texto</label>
					<textarea class="form-control" id="texto" name="texto" autofocus required>@if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['texto'] ?>" @endif</textarea>
				</div>

				<div class="form-group">
					<label for="imagem">Imagem</label>
					<input type="file" class="form-control" id="imagem" name="imagem" required>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>
				<a href="{{URL::route('painel.sustentabilidadeacaosocial.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop