<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Grupo Bignardi - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'bower_components/reset-css/reset',
		'bower_components/jquery-ui/themes/base/jquery-ui',
		'bower_components/bootstrap/dist/css/bootstrap.min',
		'bower_components/css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('bower_components/modernizr/modernizr', 'bower_components/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('bower_components/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Grupo Bignardi</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li @if(str_is('painel.paginainicial*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.paginainicial.index')}}" title="Home do Site">Home do Site</a>
						</li>

						<li class="dropdown">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Grupo <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.grupo.index')}}" title="Textos">Textos</a></li>
								<li><a href="{{URL::route('painel.grupomissao.index')}}" title="Missão,Visão e Valores">Missão,Visão e Valores</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Prêmios <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.premios.index')}}" title="Prêmios">Prêmios</a></li>
								<li><a href="{{URL::route('painel.premiostexto.index')}}" title="Texto Introdutório">Texto Introdutório</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Certificações <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.certificacoes.index')}}" title="Texto Introdutório">Texto Introdutório</a></li>
								<li><a href="{{URL::route('painel.certificacoesobtidas.index')}}" title="Certificações">Certificacoes</a></li>
							</ul>
						</li>

						<li class="dropdown">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Sustentabilidade <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.sustentabilidade.index')}}" title="Texto Introdutório">Texto Introdutório</a></li>
								<li><a href="{{URL::route('painel.sustentabilidadeacaosocial.index')}}" title="Texto de Ação Social">Texto de Ação Social</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.contato*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
						</li>

						<li class="dropdown">
							<a href="#" title="Com Dropdown" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="" title="Usuários do Painel">Usuários</a></li>
								<li><a href="{{URL::route('painel.logout')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'bower_components/jquery-ui/ui/minified/jquery-ui.min',
				'bower_components/bootbox/bootbox',
				'bower_components/ckeditor/ckeditor',
				'bower_components/ckeditor/adapters/jquery',
				'bower_components/bootstrap/dist/js/bootstrap.min',
				'assets/js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'bower_components/jquery-ui/ui/minified/jquery-ui.min',
				'bower_components/bootbox/bootbox',
				'bower_components/ckeditor/ckeditor',
				'bower_components/ckeditor/adapters/jquery',
				'bower_components/bootstrap/dist/js/bootstrap.min',
				'bower_components/painel/painel',
				'assets/js/painel/painel'
			))?>
		@endif

	</body>
</html>