@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Textos da Página Inicial
        </h2>  

		{{ Form::open( array('route' => array('painel.premios.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="titulo">Título</label>
					<input type="text" class="form-control" id="titulo" autofocus name="titulo" value="{{$registro->titulo}}"required>
				</div>

				<div class="form-group">
					<label for="subtitulo">Subtítulo</label>
					<input type="text" class="form-control" id="subtitulo" name="subtitulo" value="{{$registro->subtitulo}}"required>
				</div>

				<div class="form-group">
					<label for="slug">Slug</label>
					<input type="text" class="form-control" id="slug" name="slug" value="{{$registro->slug}}" required>
				</div>

				<div class="form-group">
					<label for="olho">Olho</label>
					<textarea class="form-control" id="olho" name="olho" required>{{$registro->olho}}</textarea>
				</div>

				<div class="form-group">
					<label for="texto">Texto</label>
					<textarea class="form-control" id="texto" name="texto" required>{{$registro->texto}}</textarea>
				</div>

				<div class="form-group">
					<label for="date">Data</label>
					<input type="text" class="form-control datepicker" id="data" name="data" value="{{$registro->data}}" required>
				</div>

				<div class="form-group">
					@if($registro->imagem)
						Imagem Atual<br>
						<img src="assets/images/premios/{{$registro->imagem}}"><br>
					@endif
					<label for="imagem">Trocar Imagem</label>
					<input type="file" class="form-control" id="imagem" name="imagem">
				</div>

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.premios.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>
			</div>
		</form>
    </div>
    
@stop