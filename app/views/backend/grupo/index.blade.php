@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Grupo - Textos
        @if(count($registros)<=4)
            <a href='{{ URL::route('painel.grupo.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto grupo</a>
        @endif﻿
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='grupo'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Texto</th>
                <th>Imagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row">
                <td>{{ $registro->titulo}}</td>
				<td>{{ $registro->texto}}</td>
                <td><img src='assets/images/grupo/{{ $registro->imagem }}' style='max-width:150px'></td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.grupo.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.grupo.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop