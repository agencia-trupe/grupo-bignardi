@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Texto de Visão,Missão e Valores
        @if(count($registros)<3)
            <a href='{{ URL::route('painel.grupomissao.create') }}' class='btn btn-success btn-sm pull-right'><span class='glyphicon glyphicon-plus-sign'></span> Adicionar Texto</a>
        @endif﻿
    </h2>

    <table class='table table-striped table-bordered table-hover table-sortable' data-tabela='clientes'>

        <thead>
            <tr>
				<th>Título</th>
                <th>Texto</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row">
				<td>{{ $registro->titulo}}</td>
				<td>{{ $registro->texto}}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.grupomissao.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
						{{ Form::open(array('route' => array('painel.grupomissao.destroy', $registro->id), 'method' => 'delete')) }}
							<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
						{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop