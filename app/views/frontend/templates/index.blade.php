<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2014 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Grupo Bignardi - Solução em Papel</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'bower_components/reset-css/reset',
		'bower_components/jquery-ui/themes/base/jquery-ui',
		'assets/css/style',
		'bower_components/pure/pure-min',
		'assets/css/estilockeditor',
		'bower_components/pure/grids-responsive-min',
		'bower_components/pure/grids-responsive-old-ie-min'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('bower_components/modernizr/modernizr', 'bower_components/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('bower_components/modernizr/modernizr'))?>
	@endif

</head>
	<body>
		<!--
		<header>
			<div class="menuHomeAlinhado">
				<div class="pure-g">
					<div class="pure-u-17-24">
						<div class="pure-menu pure-menu-open pure-menu-horizontal mudarFundoDiv">
	    					<ul>
		    				<li><a href="home" title="HOME" class="imagemMenu"><img src="assets/images/front/home/logo-grupo-bignardi.png" alt="Previz - Consultoria Previdenciária" class="pure-img"/></a></li>

		        			<li><a class="menuLinks" href="grupo" title="GRUPO" @if(str_is('grupo*', Route::currentRouteName())) class="ativo" @endif>GRUPO</a></li>

		        			<li><a class="menuLinks" href="premios" title="PRÊMIOS" @if(str_is('premios*', Route::currentRouteName())) class="ativo" @endif>PRÊMIOS</a></li>

		        			<li><a class="menuLinks" href="certificacoes" title="CERTIFICAÇÕES" @if(str_is('certificacoes*', Route::currentRouteName())) class="ativo" @endif>CERTIFICAÇÕES</a></li>

		        			<li><a class="menuLinks" href="sustentabilidade" title="SUSTENTABILIDADE" @if(str_is('sustentabilidade*', Route::currentRouteName())) class="ativo" @endif>SUSTENTABILIDADE</a></li>

		        			<li><a href="contato" title="CONTATO" class="menuLinks" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif>CONTATO</a></li>
	    					</ul>
						</div>
					</div>
				</div>
			</div>
		</header>
		-->

		<header>
			<div class="menuHomeAlinhado">
				<div class="pure-g">
					<div class="pure-u-17-24">
						<div class="pure-menu pure-menu-open pure-menu-horizontal mudarFundoDiv">
	    					<ul>
		    				<li><a href="home" title="HOME" class="imagemMenu"><img src="assets/images/front/home/logo-grupo-bignardi.png" alt="Previz - Consultoria Previdenciária" class="pure-img"/></a></li>

		        			<li><a class="menuLinks" href="grupo" title="GRUPO" @if(str_is('grupo*', Route::currentRouteName())) class="ativo" @endif>GRUPO</a></li>

		        			<li><a class="menuLinks" href="premios" title="PRÊMIOS" @if(str_is('premios*', Route::currentRouteName())) class="ativo" @endif>PRÊMIOS</a></li>

		        			<li><a class="menuLinks" href="certificacoes" title="CERTIFICAÇÕES" @if(str_is('certificacoes*', Route::currentRouteName())) class="ativo" @endif>CERTIFICAÇÕES</a></li>

		        			<li><a class="menuLinks" href="sustentabilidade" title="SUSTENTABILIDADE" @if(str_is('sustentabilidade*', Route::currentRouteName())) class="ativo" @endif>SUSTENTABILIDADE</a></li>

		        			<li><a href="contato" title="CONTATO" class="menuLinks" @if(str_is('contato*', Route::currentRouteName())) class="ativo" @endif>CONTATO</a></li>
	    					</ul>
						</div>
					</div>
				</div>
			</div>
		</header>

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="rodape">
				<div class="pure-g">
					@foreach ($contato as $contato) 
						<div class="pure-u-15-24">
							<p><b>SAC</b> {{$contato->telefone}}</p>
							<p>{{$contato->enderecorodape}}</p>
						</div>
						<div class="pure-u-9-24 alinhamentoDireita">
							<p>{{Date('Y')}} Bignardia Papéis - Todos os direitos reservados </p>
							<a href="http://trupe.net/" target="_blank" title="Criação de Sites: Trupe Agência Criativa"><p>Criação de Sites: Trupe Agência Criativa</p></a>
						</div>
					@endforeach
				</div>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'bower_components/jquery-ui/ui/minified/jquery-ui.min',
				'bower_components/jquery/dist/jquery.min',
				'bower_components/bootbox/bootbox',
				'bower_components/ckeditor/ckeditor',
				'bower_components/ckeditor/adapters/jquery',
				'bower_components/bootstrap/dist/js/bootstrap.min',
				'assets/js/front',
				'assets/js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'bower_components/jquery-ui/ui/minified/jquery-ui.min',
				'bower_components/jquery/dist/jquery.min',
				'bower_components/bootbox/bootbox',
				'bower_components/ckeditor/adapters/jquery',
				'bower_components/bootstrap/dist/js/bootstrap.min',
				'bower_components/painel/painel',
				'assets/js/front',
				'assets/js/painel/painel'
			))?>
		@endif

	</body>
</html>