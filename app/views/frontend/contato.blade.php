@section('conteudo')
	<main>
		<div class="contato">
			<div class="pure-g">
				<div class="pure-u-2-5">
					@foreach($contato as $contato)
						<p class="titulo">Contato</p>
						<div class="pure-u-2-24 espacoEntreInformacoes">
							<img src="assets/images/front/contato/local.png"/>
						</div>
						<div class="pure-u-20-24 espacoEntreInformacoes">
							<p class="texto">{{$contato->endereco}}</p>
						</div>
						<div class="pure-u-2-24 espacoEntreInformacoes">
							<img src="assets/images/front/contato/email.png"/>
						</div>
						<div class="pure-u-20-24 espacoEntreInformacoes">
							<p class="texto">{{$contato->email}}</p>
						</div>
						<div class="pure-u-2-24 espacoEntreInformacoes">
							<img src="assets/images/front/contato/telefone.png"/>
						</div>
						<div class="pure-u-20-24 espacoEntreInformacoes">
							<p class="texto">{{$contato->telefone}}</p>
						</div>
					@endforeach
				</div>
				<div class="pure-u-3-5">
					@if(Session::has('sucesso'))
			    	   <div class="pure-u-1 mensagemDeSucesso">{{ Session::get('mensagem') }}</div>
			        @endif

			    	@if($errors->any())
			    		<div class="pure-u-1 mensagemDeErro">{{ $errors->first() }}</div>
			    	@endif

					<form action="{{URL::route('contato.enviar')}}" method="post" enctype="multipart/form-data">
				        <div class="form-group">
							<label for="nome">Nome</label>
							<input type="text" class="form-control" id="nome" autofocus name="nome" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['nome'] ?>" @endif required />
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" class="form-control" id="email" autofocus name="email" @if(Session::has('formulario')) value="<?php $s=Session::get('formulario'); echo $s['email'] ?>" @endif required />
						</div>

						<div class="form-group">
							<label for="mensagem" class="mensagem">Mensagem</label>
							<textarea class="form-control" id="mensagem" name="mensagem" required>@if(Session::has('formulario')) <?php $s=Session::get('formulario'); echo $s['mensagem'] ?> @endif</textarea>
						</div>

						<button type="submit" title="enviar" class="botaoEnviarMensagem">ENVIAR ></button>
					</form>
				</div>
			</div>
			<div class="pure-g fundoVerde">
				<div class="pure-u-1">
				</div>
			</div>
		</div>
	</main>
@stop