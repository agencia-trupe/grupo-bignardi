@section('conteudo')
	<main>

		<article class="asTresEmpresas">
			<div class="asTresEmpresasCentralizar">
				<div class="pure-g">
					@foreach ($paginainicial as $paginaInicial)
						<div class="pure-u-1">
							<p class="titulo"> <?php echo $paginaInicial->frasehome;?></p>
						</div>
						<div class="pure-u-1">
				    		<p><?php echo $paginaInicial->subfrasehome;?></p>
				    	</div>

				    	<div class="pure-u-6-24">
							<a href="grupo">
								<img src="assets/images/front/home/bignardi-papeis.png" alt="Previz - Consultoria Previdenciária" class="pure-img">
							</a>
				    		<p class="textoEmpresa"><?php echo $paginaInicial->textochamadabignardi;?></p>
				    		<a href="grupo" class="leiaMais">+</a>
				    	</div>

				    	<div class="pure-u-6-24">
							<a href="grupo">
								<img src="assets/images/front/home/logo-jandaia.png" alt="Previz - Consultoria Previdenciária" class="pure-img">
							</a>
				    		<p class="textoEmpresa"><?php echo $paginaInicial->textochamadajandaia;?></p>
				    		<a href="grupo" class="leiaMais">+</a>
				    	</div>

				    	<div class="pure-u-6-24">
						    <a href="grupo">
								<img src="assets/images/front/home/logo-jandaia-atacado.png" alt="Previz - Consultoria Previdenciária" class="pure-img">
							</a>
						    <p class="textoEmpresa"><?php echo $paginaInicial->textochamadaatacadao;?></p>
						    <a href="grupo" class="leiaMais">+</a>
				    	</div>

					@endforeach
				</div>
			</div>
			
		</article>

		<article class="fundoTextoReferencia">
			<div class="textoDeReferenciaComResponsivo">
				<div class="pure-g">	
			    	<div class="pure-u-3-5">
			    		@foreach ($paginainicial as $paginaInicial)
			    			<p class="tituloReferencia"> <?php echo $paginaInicial->tituloreferencia;?></p>
			    			<p><?php echo $paginaInicial->textoreferencia;?></p>
			    		@endforeach
			    	</div>
			   		<div class="pure-u-1-5">
			   			<img src="assets/images/front/home/referencia-nacional.png" alt="Previz - Consultoria Previdenciária">
			   		</div>
		   		</div>
			</div>
		</article>
	</main>
@stop