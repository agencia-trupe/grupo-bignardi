@section('conteudo')
	<main class="certificacoes">
		<div class="pure-g">
			<div class="pure-u-24-24" id="certificacaoTituloIntrodutorio">
				<p>Certificações</p>
			</div>
			<div class="pure-u-24-24" id="certificacaoTextoIntrodutorio">
				@foreach($certificacoes as $certificacoes)
					{{$certificacoes->texto}}
				@endforeach
			</div>
			@foreach($certificacoesobtidas as $certificacoesobtidas)
				<div class="pure-g">
					<div class="pure-u-17-24">
						<p class="certificacoTitulo"> {{$certificacoesobtidas->titulo}} </p>
						{{$certificacoesobtidas->texto}}
					</div>
					<div class="pure-u-5-24">
						<img alt="{{$certificacoesobtidas->titulo}}" src="assets/images/certificacoesobtidas/{{$certificacoesobtidas->imagem}}" class="pure-img"/>
					</div>
				</div>
			@endforeach
		</div>
	</main>
	<div class="pure-g fundoVerde">
		<div class="pure-u-1">
		</div>
	</div>
@stop