@section('conteudo')
	<main>
		<div class="grupos">
			<div class="pure-g">
				<div class="pure-u-17-24">
					<p class="tituloGrupo">Grupo</p>
					<img data-id="0" src="assets/images/front/grupo/grupo-bignardi-2.png" alt="Grupo Bignardi - Soluções em Papel" class="pure-img imagensSelecionaveis"/>			
					<img data-id="1" src="assets/images/front/grupo/bignardi-papeis.png" alt="Bignardi Papéis" class="pure-img imagensSelecionaveis"/>
					<img data-id="2" src="assets/images/front/grupo/jandaia.png" alt="Jandaia" class="pure-img imagensSelecionaveis"/>
					<img data-id="3" src="assets/images/front/grupo/jandaia-atacado.png" alt="Jandaia - Atacado em Papelaria" class="pure-img imagensSelecionaveis"/>

					<?php
						$primeiroValor = 0;
					?>
						@foreach ($grupo as $grupo)
							@if($primeiroValor==0)
								<div id="content0" class="pure-g show empresasjavascript">
									<div class="pure-u-17-24 textoEmpresa">
										{{$grupo->texto}}
										<div class="pure-g-">
											<div class="pure-u-1 linkExterno">
												<a href="http://{{$grupo->link}}" target="_blank">PARA SABER MAIS, VISITE O WEBSITE DA {{$grupo->titulo}}</a>
											</div>
										</div>
									</div>
									<div class="pure-u-7-24">
										<img src="assets/images/grupo/{{$grupo->imagem}}" alt="{{$grupo->titulo}}" class="imagemEmpresa pure-img"/>
									</div>
								</div>
								<?php
									$primeiroValor++;
								?>
							@else
								<div id="content{{$primeiroValor++}}" class="pure-g hide empresasjavascript">
									<div  class="pure-u-17-24 textoEmpresa">
										{{$grupo->texto}}
										<div class="pure-g-">
											<div class="pure-u-1 linkExterno">
												<a href="http://{{$grupo->link}}" target="_blank">PARA SABER MAIS, VISITE O WEBSITE DA {{$grupo->titulo}}</a>
											</div>
										</div>
									</div>
									<div class="pure-u-7-24">
										<img src="assets/images/grupo/{{$grupo->imagem}}" alt="{{$grupo->titulo}}" class="imagemEmpresa pure-img"/>
									</div>
								</div>
							@endif
						@endforeach
					

				</div>
				<div class="pure-u-1">
					<div class="pure-g">
						<div class="pure-u-17-24">
							<div class="pure-g">
								@foreach($grupomissao as $grupomissao)
									<div class="pure-u-7-24">
										<p class="titulo">{{$grupomissao->titulo}}</p>
										<div class="texto">{{$grupomissao->texto}}</div>
									</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
@stop