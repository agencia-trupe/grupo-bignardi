@section('conteudo')
	<div class="sustentabilidade ">
		<div class="pure-g">
			@foreach($sustentabilidade as $sustentabilidade)
				<div class="pure-u-17-24">
					<p class="titulo">Sustentabilidade</p>
					{{$sustentabilidade->texto}}
				</div>
				<div class="pure-u-7-24">
					<img alt="{{$sustentabilidade->titulo}}" src="assets/images/sustentabilidade/{{$sustentabilidade->imagem}}" class="pure-img"/>
				</div>
			@endforeach
		</div>
	</div>
	<div class="acaosocial">
		<div class="pure-g">
			@foreach($sustentabilidadeacaosocial as $sustentabilidadeacaosocial)
				<div class="pure-u-17-24">
					<p class="titulo">{{$sustentabilidadeacaosocial->titulo}}</p>
					{{$sustentabilidadeacaosocial->texto}}
				</div>
				<div class="pure-u-5-24">
					<img class="pure-img" alt="{{$sustentabilidadeacaosocial->titulo}}" src="assets/images/sustentabilidadeacaosocial/{{$sustentabilidadeacaosocial->imagem}}" class="pure-img" />
				</div>
			@endforeach
		</div>
	</div>
@stop