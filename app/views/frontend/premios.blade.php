@section('conteudo')
	<main>
		<div class="premios">
			<div class="pure-g">
				<div class="pure-u-17-24">
					<p class="tituloIntrodutorio">Prêmios</p>
					<div class="textoIntrodutorio">
						@foreach($premiostexto as $premiostexto) 
							{{$premiostexto->texto}}
						@endforeach
					</div>
					@foreach($premios as $premio)
						<div class="pure-g">
							<div class="pure-u-17-24">
								<span class="tituloPremio">{{$premio->titulo}}</span>
								<span class="dataPremio">{{$premio->data}}</span>
								<p class="subtituloPremio">{{$premio->subtitulo}}</p>
								<div class="textoPremio">
									{{$premio->olho}}
								</div>
								<div class="textoPremioEscondido hide">
									{{$premio->texto}}
								</div>
							</div>
							<div class="pure-u-5-24">
								<img src="assets/images/premios/{{$premio->imagem}}" alt="{{$premio->titulo}}" class="pure-img"/>
								<div class="leiaMais" href="">VER MAIS +</div>
							</div>
						</div>
					@endforeach
				 {{ $premios->links() }}
				</div>
			</div>
		</div>
	</main>
@stop