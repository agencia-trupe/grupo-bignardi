<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('grupo', array('as' => 'grupo', 'uses' => 'HomeController@grupo'));
Route::get('premios', array('as' => 'premios', 'uses' => 'HomeController@premios'));
Route::get('certificacoes', array('as' => 'certificacoes', 'uses' => 'HomeController@certificacoes'));
Route::get('sustentabilidade', array('as' => 'sustentabilidade', 'uses' => 'HomeController@sustentabilidade'));
Route::get('contato', array('as' => 'contato', 'uses' => 'HomeController@contato'));
Route::post('contato', array('as' => 'contato.enviar', 'uses' => 'HomeController@enviar'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));
Route::post('painel/login',array('as' => 'painel.login', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.logout', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{
    Route::resource('paginainicial', 'Painel\PaginaInicialController');
    Route::resource('grupo', 'Painel\GrupoController');
    Route::resource('grupomissao', 'Painel\GrupoMissaoController');
    Route::resource('premios', 'Painel\PremiosController');
    Route::resource('premiostexto', 'Painel\PremiosTextoController');
    Route::resource('certificacoes', 'Painel\CertificacoesController');
    Route::resource('certificacoesobtidas', 'Painel\CertificacoesObtidasController');
    Route::resource('sustentabilidade', 'Painel\SustentabilidadeController');
    Route::resource('sustentabilidadeacaosocial', 'Painel\SustentabilidadeAcaoSocialController');
    Route::resource('contato', 'Painel\ContatoController');
});

Route::post('ajax/gravaOrdem', array('uses' => 'Painel\AjaxController@gravaOrdem'));
