<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Premios;

class PremiosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.premios.index')->with('registros', Premios::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.premios.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Premios;

		$object->titulo= Input::get('titulo');
		$object->subtitulo = Input::get('subtitulo');
		$object->slug = Input::get('slug');
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');
		$data = substr(Input::get('data'),6,4).'-'.substr(Input::get('data'),3,2).'-'.substr(Input::get('data'),0,2);
		$object->data= $data;
		$imagem = Thumb::make('imagem', 180, 120, 'premios/', false, '#FFFFFF', true);
		if($imagem) $object->imagem = $imagem;

		try {
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.premios.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar texto!'));	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.premios.edit')->with('registro', premios::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Premios::find($id);
		
		$object->titulo= Input::get('titulo');
		$object->subtitulo = Input::get('subtitulo');
		$object->slug = Input::get('slug');
		$object->olho = Input::get('olho');
		$object->texto = Input::get('texto');

		if(($object->data) != (Input::get('data')) ){
			$data = substr(Input::get('data'),6,4).'-'.substr(Input::get('data'),3,2).'-'.substr(Input::get('data'),0,2);
			$object->data= $data;
		}
		
		$imagem = Thumb::make('imagem', 180, 120, 'premios/', false, '#FFFFFF', true);
		if($imagem) {
			File::delete('assets/images/premios/'.$object->imagem);
			$object->imagem = $imagem;
		}
		
		try {
			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.premios.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar texto!'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Premios::find($id);
		File::delete('assets/images/premios/'.$object->imagem);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.premios.index');
	}

}