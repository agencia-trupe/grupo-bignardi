<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, CertificacoesObtidas;

class CertificacoesObtidasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the ersource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.certificacoesobtidas.index')->with('registros', CertificacoesObtidas::orderBy('ordem', 'ASC')->get());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.certificacoesobtidas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new CertificacoesObtidas;

		$object->titulo= Input::get('titulo');
		$object->texto= Input::get('texto');
		$imagem = Thumb::make('imagem', 170, 120, 'certificacoesobtidas/', false, '#FFFFFF', false);
		if($imagem) $object->imagem = $imagem;

		try {
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Certificação criada com sucesso.');
			return Redirect::route('painel.certificacoesobtidas.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar certificação!'));	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.certificacoesobtidas.edit')->with('registro', CertificacoesObtidas::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = CertificacoesObtidas::find($id);

		$object->titulo= Input::get('titulo');
		$object->texto = Input::get('texto');
		$imagem = Thumb::make('imagem', 250, 200, 'certificacoesobtidas/', false, '#FFFFFF', false);
		if($imagem) {
			File::delete('assets/images/certificacoesobtidas/'.$object->imagem);
			$object->imagem = $imagem;
		}

		try {
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Certificação atualizada com sucesso.');
			return Redirect::route('painel.certificacoesobtidas.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar certificação!'));	
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = CertificacoesObtidas::find($id);
		$object->delete();
		File::delete('assets/images/certificacoesobtidas/'.$object->imagem);

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.certificacoesobtidas.index');
	}

}