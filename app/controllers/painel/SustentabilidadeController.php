<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, Sustentabilidade;

class SustentabilidadeController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.sustentabilidade.index')
		                              ->with('registros', Sustentabilidade::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.sustentabilidade.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Sustentabilidade;

		$object->texto= Input::get('texto');
		$imagem = Thumb::make('imagem', 260, 390, 'sustentabilidade/', false, '#FFFFFF', true);
		if($imagem) $object->imagem = $imagem;

		try {
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.sustentabilidade.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar texto!'));	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.sustentabilidade.edit')->with('registro', Sustentabilidade::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Sustentabilidade::find($id);

		$object->texto = Input::get('texto');
		$imagem = Thumb::make('imagem', 260, 390, 'sustentabilidade/', false, '#FFFFFF', true);
		if($imagem) {
			File::delete('assets/images/sustentabilidade/'.$object->imagem);
			$object->imagem = $imagem;
		} 

		try {
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.sustentabilidade.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar texto!'));	
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Sustentabilidade::find($id);
		$object->delete();
		File::delete('assets/images/sustentabilidade/'.$object->imagem);

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.sustentabilidade.index');
	}

}