<?php

namespace Painel;

use View, Input, Str, Session, Redirect, Hash, Thumb, File, PaginaInicial;

class PaginaInicialController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.paginainicial.index')
		                              ->with('registros', PaginaInicial::all());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.paginainicial.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new PaginaInicial;

		$object->frasehome = Input::get('frasehome');
		$object->subfrasehome = Input::get('subfrasehome');
		$object->textochamadabignardi = Input::get('textochamadabignardi');
		$object->textochamadajandaia = Input::get('textochamadajandaia');
		$object->textochamadaatacadao = Input::get('textochamadaatacadao');
		$object->tituloreferencia = Input::get('tituloreferencia');
		$object->textoreferencia = Input::get('textoreferencia');

		try {
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.paginainicial.index');

		} catch (\Exception $e) {
			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar texto!'));	
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.paginainicial.edit')->with('registro', PaginaInicial::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = PaginaInicial::find($id);

		$object->frasehome = Input::get('frasehome');
		$object->subfrasehome = Input::get('subfrasehome');
		$object->textochamadabignardi = Input::get('textochamadabignardi');
		$object->textochamadajandaia = Input::get('textochamadajandaia');
		$object->textochamadaatacadao = Input::get('textochamadaatacadao');
		$object->tituloreferencia = Input::get('tituloreferencia');
		$object->textoreferencia = Input::get('textoreferencia');
	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.paginainicial.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = PaginaInicial::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.paginainicial.index');
	}

}