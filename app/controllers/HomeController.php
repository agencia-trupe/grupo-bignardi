<?php
	use \View, \PaginaInicial, \Grupo , \GrupoMissao ,\PremiosTexto , \Premios ,\Certificacoes , \CertificacoesObtidas, \Sustentabilidade ,\SustentabilidadeAcaoSocial , \Contato, \Mail;

	class HomeController extends BaseHomeController {

	    public function index()
		{
			$this->layout->content = View::make('frontend.home')->with('paginainicial', PaginaInicial::all());
		}

		public function grupo()
		{
			$this->layout->content = View::make('frontend.grupo')->with('grupo', Grupo::all()) -> with('grupomissao', GrupoMissao::all());
		}

		public function premios()
		{
			$this->layout->content = View::make('frontend.premios')->with('premiostexto', PremiosTexto::all()) ->with('premios', Premios::orderBy('ordem', 'ASC')->paginate(5));
		}

		public function certificacoes()
		{
			$this->layout->content = View::make('frontend.certificacoes')->with('certificacoes', Certificacoes::all()) ->with('certificacoesobtidas', CertificacoesObtidas::orderBy('ordem', 'ASC')->get());
		}

		public function sustentabilidade()
		{
			$this->layout->content = View::make('frontend.sustentabilidade')->with('sustentabilidade', Sustentabilidade::all()) ->with('sustentabilidadeacaosocial', SustentabilidadeAcaoSocial::all());
		}

		public function contato()
		{
			$this->layout->content = View::make('frontend.contato')->with('contato', Contato::all());
		}

		public function enviar()
		{
			$data['nome'] = \Input::get('nome');
			$data['email'] = \Input::get('email');
			$data['mensagem'] = \Input::get('mensagem');

			if($data['nome'] && $data['email'] && $data['mensagem']){
				\Mail::send('frontend.emails.contato', $data, function($message) use ($data)
				{
				    $message->to('allan@trupe.net', 'Grupo Bignardi')
				    		->subject('Contato via site')
				    		->bcc('allan@trupe.net')
				    		->replyTo($data['email'], $data['nome']);
				});
			}

			Session::flash('enviado', true);
			return \Redirect::route('contato');
		}
	}
?>