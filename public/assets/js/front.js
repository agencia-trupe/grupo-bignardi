$(function() {
	$('.leiaMais').click(function(){
		var textoPremioEscondido = $(this).parent().parent().find('.textoPremioEscondido');
		if(!textoPremioEscondido.hasClass('show')) {			
			// Exibe o conteúdo do texto oculto
			textoPremioEscondido.slideDown(function() {
				textoPremioEscondido.addClass('show').removeClass('hide');
				textoPremioEscondido.parent().parent().find('.pure-u-5-24').css('background-color', '#004351');
				textoPremioEscondido.parent().css('background-color', '#004351');
				textoPremioEscondido.parent().find('.textoPremio').css("color","#fff");
				textoPremioEscondido.parent().find('.textoPremioEscondido').css("color","#fff");
				$(this).parent().parent().find('.pure-u-5-24', function(){
					$(this).find('.leiaMais').html("VER MENOS -");
				});
			});
		}
		else {
			// Remove qualquer texto que esteja sendo mostrado
			$('.pure-u-17-24').find('.show').slideUp(function() {
				$(this).addClass('hide').removeClass('show');
				$(this).parent().css('background-color', '#9BAAAF');
				$(this).parent().find('.textoPremio').css("color","#004351");
				$(this).parent().find('.textoPremioEscondido').css("color","#004351");
				$(this).parent().parent().find('.pure-u-5-24').css('background-color', '#9BAAAF');
				$(this).parent().parent().find('.pure-u-5-24', function(){
					$(this).find('.leiaMais').html("VER MENOS +");
				});
			});
		}

	});

	$('.imagensSelecionaveis').click(function(){
		var id = $(this).data("id");
		var id0 = $('img[data-id="0"]');
		var id1 = $('img[data-id="1"]');
		var id2 = $('img[data-id="2"]');
		var id3 = $('img[data-id="3"]');
		if(id == 3) {
			// Chama a imagem colorida
			$(this).attr('src', 'assets/images/front/grupo/jandaia-atacado-2.png');
			// Transforma todas as outras em imagens p&b
			id2.attr('src', 'assets/images/front/grupo/jandaia.png');
			id1.attr('src', 'assets/images/front/grupo/bignardi-papeis.png');
			id0.attr('src', 'assets/images/front/grupo/grupo-bignardi.png');
		}
		else if(id == 2) {
			// Chama a imagem colorida
			$(this).attr('src', 'assets/images/front/grupo/jandaia-2.png');
			// Transforma todas as outras em imagens p&b
			id3.attr('src', 'assets/images/front/grupo/jandaia-atacado.png');
			id1.attr('src', 'assets/images/front/grupo/bignardi-papeis.png');
			id0.attr('src', 'assets/images/front/grupo/grupo-bignardi.png');
		}
		else if(id == 1) {
			// Chama a imagem colorida
			$(this).attr('src', 'assets/images/front/grupo/bignardi-papeis-2.png');
			// Transforma todas as outras em imagens p&b
			id3.attr('src', 'assets/images/front/grupo/jandaia-atacado.png');
			id2.attr('src', 'assets/images/front/grupo/jandaia.png');
			id0.attr('src', 'assets/images/front/grupo/grupo-bignardi.png');
		}
		else if(id == 0) {
			// Chama a imagem colorida
			$(this).attr('src', 'assets/images/front/grupo/grupo-bignardi-2.png');
			// Transforma todas as outras em imagens p&b
			id3.attr('src', 'assets/images/front/grupo/jandaia-atacado.png');
			id2.attr('src', 'assets/images/front/grupo/jandaia.png');
			id1.attr('src', 'assets/images/front/grupo/bignardi-papeis.png');
		}
        $('.empresasjavascript').addClass('hide').removeClass('show');
        $('#content'+id).addClass('show').removeClass('hide');
	});
	
});