/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	//config.extraPlugins = 'panel,floatpanel,richcombo,stylescombo';
	//config.stylesSet = 'custom_styles';
	config.stylesSet = 'meus_estilos_customizados';
	config.extraPlugins = 'stylescombo';


	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [];

	config.toolbar = [
	    [ 'Bold', 'Italic', 'Underline' ],
	    [ 'NumberedList', 'BulletedList' ],
	    [ 'Link', 'Unlink' ],
	    [ 'FontSize', 'TextColor' ],
	    [ 'Styles' ]
	];

	// Remove some buttons, provided by the standard plugins, which we don't
	// need to have in the Standard(s) toolbar.
	config.removeButtons = 'Subscript,Superscript';

	// Se the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Make dialogs simpler.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	//Configuração de CSS externo para estilização de estilos(Usado o "Destaque")
	config.contentsCss = '/laravel/public/assets/css/estilockeditor.css';
};
